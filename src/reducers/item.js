import {UPDATE_STATUS,ADD_ITEM, UPDATE_ITEM,DELETE_ITEM} from '../actions/index'
const initProduct = [
    {
        id:1,
        title:'Eat',
        status: 0
    },
    {
        id:2,
        title:'Sleep',
        status: 1
    },
    {
        id:3,
        title:'Swim',
        status: 1
    },
    {
        id:4,
        title:'walking',
        status: 1
    },
    {
        id:5,
        title:'dead',
        status: 0
    },
    {
        id:6,
        title:'Fly',
        status: 0
    },
]

const item = (state = initProduct, action) => {
    switch (action.type) {
        case UPDATE_STATUS:{
            const newList = state.map(ele=>{
                if(ele.id === action.payload.id){
                    ele.status = action.targetColumn
                }
                return ele
            })
            return newList
        }
        case ADD_ITEM:{
            let maxId = 0
            state.forEach(ele=>{
                 if(ele.id > maxId){
                     maxId=ele.id
                 }
             })
            const Item = {
                id: maxId+1,
                title: action.payload,
                status:0
            }
            let newList = [...state]
            newList.push(Item)
            return newList
        }
        case UPDATE_ITEM:{
            console.log("day la action edit",action)
            const newList = state.map(ele=>{
                if(ele.id === action.id){
                    ele.title = action.payload
                }
                return ele
            })
            console.log("list column sau khi xu li",newList)
            return newList
        }
        case DELETE_ITEM : {
            const newList = state.filter(ele=>{
                return ele.id !== action.id
            })
            return newList
        }
        default: return state
    }
}
export default item