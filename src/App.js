
import logo from './logo.svg';
import './App.css';
import { connect } from 'react-redux';
import React, { useState, useEffect } from 'react';
import { Container, Draggable } from "react-smooth-dnd";
import { applyDrag } from "./utils/utils";
import {updateStatusItem,addItem,removeItem,updateItem} from "./actions/index"
const App = (props) => {
  const [targetColumn,setTargetColumn]= useState(null)
  const [newItem,setNewItem]= useState('')
  const [onUpdate,setOnUpdate]= useState(null)
  const [edit,setEdit] = useState('')
  useEffect(() => {
    if(onUpdate){
      setEdit(onUpdate.title)

    }
  }, [onUpdate])
  const listItem = props.listItem
  console.log("Day la list item ban dau ",listItem)
  const todo = listItem.filter(ele => {
    return ele.status === 0
  })
  const done = listItem.filter(ele => {
    return ele.status === 1
  })
  
  const onColumnDrop = (dropResult,ele) => {
    if(dropResult.removedIndex !== null || dropResult.addedIndex !==null){
      if(dropResult.payload.status !== targetColumn){
        props.updateStatusItem(dropResult.payload,targetColumn)
      }
    }
  }
  const handleAddItem =()=>{
    props.addItem(newItem)
    setNewItem('')
  }
  return (
    <div className="App">
     <Container
        orientation="horizontal"
        onDrop={(e)=>onColumnDrop(e)}
        dragHandleSelector=".column-drag-handle"
        className="container"
      >
          <Draggable className="todo-container column-drag-handle" key="x">
                <div className='title-todo'>
                  Todo
                </div>
                {
                  todo.map(ele=>{
                    return (
                      <Container
                        groupName="col"
                        onDrop={(e)=>onColumnDrop(e,ele)}
                        getChildPayload={index =>ele}
                        dragClass="card-ghost"
                        dropClass="card-ghost-drop"
                        onDragEnter={(data) => {
                          setTargetColumn(0)
                        }}
                      >
                        <Draggable key={ele.id} className="item-todo" onClick={()=>setOnUpdate(ele)}>
                            <div className="text-title">
                              {ele.title}
                            </div>
                          {/* {onUpdate && <input value={ele.title} className="value-title" onChange={()=>{}}/> } */}
                        </Draggable>
                          <div 
                            className="button-remove"
                            onClick={()=>props.removeItem(ele.id)} 
                          >
                              remove
                          </div>
                      </Container>
                    )
                  })
                }
                <div className='add-container'>
                  <input type="text" placeholder='typing after enter to add item' 
                      className="input-todo" 
                      onChange={(value)=>{
                        setNewItem(value.target.value)
                      }}
                      onKeyPress={e=>{
                        if(e.key === 'Enter'){
                          handleAddItem()
                        }
                      }}
                      value={newItem}
                  />
                </div>
                { onUpdate && <div className="edit-container">
                    <span>Edit:</span> 
                    <input type="text" 
                      className="input-edit" 
                      onChange={(value)=>{
                        setEdit(value.target.value)
                      }}
                      onKeyPress={e=>{
                        if(e.key === 'Enter'){
                          props.updateItem(edit,onUpdate.id)
                          setOnUpdate(null);
                          setEdit('')
                        }
                      }}
                      value={edit}
                  />
                </div>}
                
          </Draggable>
          <Draggable className="done-container column-drag-handle" key="xc">
                <div className='title-done'>
                  Done
                </div>
                {
                  done.map(ele=>{
                    return (
                      <Container
                        groupName="col"
                        onDrop={(e)=>onColumnDrop(e,ele)}
                        getChildPayload={index =>ele}
                        dragClass="card-ghost"
                        dropClass="card-ghost-drop"
                        onDragEnter={(data) => {
                          setTargetColumn(1)
                        }}
                      >
                        <Draggable key={ele.id} className="item-done">
                          {ele.title}
                        </Draggable>
                      </Container>
                    )
                  })
                }
          </Draggable>
      </Container> 
      <style jxs>
        {`
          .App{
            display:flex;
            justify-content:center;
            margin-top:48px;
          }
          .container{
            display:flex;
            width:80%;
            justify-content:space-between;
            gap:64px;
          }
          .todo-container{
            display:flex;
            flex-direction:column;
            align-item:center;
            width:100%;
            // border-right:1px solid black;
          }
          .done-container{
            display:flex;
            flex-direction:column;
            justify-content:center;
            align-item:center;
            width:100%;
          }
          .title-todo, .title-done{
            display:flex;
            justify-content:center;
            align-item:center;
            width:100%;
            font-weight:bold;
          }
          .item-todo, .item-done{
            display:flex !important;
            flex-direction:column;
            justify-content:center;
            height:48px;
            width:100%;
            background:#C4C4C4;
            margin-top:12px;
            cursor:pointer;
          }
          .input-todo{
            margin-top:32px;
            height:44px;
            widthh:96px;
          }
          .value-title{
            height:44px;
            padding-left:40% !important;
            background:grey;
          }
          .edit-container{
            margin-top:32px;
          }
          .button-remove{
            margin-top:12px;

            display:flex;
            flex-direction:column;
            justify-content:center;
            height:32px;
            width:96px;
            border:1px solid black;
            border-radius: 4px;
            cursor:pointer;
            background:#1B2833;
            color: #FFFFFF;
          }
          .button-remove:hover{
            opacity:0.9;
          }
         
          .text-title{
            display:flex;
            flex-direction:column;
            justify-content:center;
          }
          .input-edit{

          }
        `}
      </style>
      <style jxs global>
        {`
          .smooth-dnd-container{
            width:100%;
            display:flex !important;
            justify-content:space-between;
            gap:32px;
          }
          .smooth-dnd-container.vertical{
            width:100%;
            display:flex !important;
            justify-content:space-between;
            gap:32px;
          }
        `}
      </style>
    </div>
  );
}
const mapStateToProps = (state) => ({
  listItem: state.item
});
const mapDispatchToProps = (dispatch) => ({
  updateStatusItem :(ele,targetColumn) =>{
    dispatch(updateStatusItem(ele,targetColumn))
  },
  addItem :(ele) =>{
    dispatch(addItem(ele))
  },
  removeItem:(id)=>{
    dispatch(removeItem(id))
  },
  updateItem:(ele,id)=>{
    dispatch(updateItem(ele,id))
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);

