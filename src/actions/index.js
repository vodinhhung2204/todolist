export const UPDATE_STATUS = 'UPDATE_STATUS';
export const ADD_ITEM = 'ADD_ITEM';
export const UPDATE_ITEM = 'UPDATE_ITEM';
export const DELETE_ITEM = 'DELETE_ITEM';

export const updateStatusItem = (ele,targetColumn) => async (dispatch) => {
    dispatch({
        type: UPDATE_STATUS,
        payload: ele,
        targetColumn:targetColumn
      });
  };

export const addItem = (ele) => async (dispatch) => {
    dispatch({
        type: ADD_ITEM,
        payload: ele,
    });
};

export const updateItem = (newItem,id) => async (dispatch) => {
    dispatch({
        type: UPDATE_ITEM,
        payload: newItem,
        id
    });
};
export const removeItem = (id) => async (dispatch) => {
    dispatch({
        type: DELETE_ITEM,
        id
    });
};
